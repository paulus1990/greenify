#######################################   
# Greenify, paint it green   
#######################################   
Author: Paulus Nunnink <paulnunnink@gmail.com>   
Version: 0.1   
   
Description:   
Gracefully shutdown Qemu VM's when inactive    
using QMP (http://wiki.qemu.org/QMP).   
   
Requirements:   
-Requires working VNC with generated password to detect inactivity    
   
Install:   
sudo apt-get install vncsnapshot
sudo apt-get install tcpdump
Make greenify executable and cron it.   
   
sudo crontab -e   
   
then enter:   
*/10 * * * * /path/to/script   
   
Default recommended settings:    
-run every 10 min.   
   
Extra:   
-Greenify needs input file in following format:   
 ip telnetport vncport vncpassfile 
-By default Greenify uses /tmp to store some variables.
